
	$(document).ready(function() {
		$.getJSON( "http://havis.um-me.com/api/pages", function(data) {
			var items = [];
			
			$.each( data, function(key, item) {
				if (key == 0) { var active = ' first active'; } else { var active = ''; }
				
				if (item.body) { var body = '<div class="body">' + item.body + '</div>'; } else { var body = ''; }

				if (item.nid == 16 || item.nid == 17) {
				
					var html = '<div class="page' + active + '" id="page-' + item.nid + '">' +
											body + 
											'<img src="' + item.image + '" class="shadow img"  >' + 
											'</div>';					
				} else {
					var html = '<div class="page' + active + '" id="page-' + item.nid + '">' +
											'<img src="' + item.image + '" class="shadow img"  >' + 
											'</div>';					
				}

				$('#about #pages').append(html);
				
				if (key == 0) { var active_link = ' class="first active_link"'; } else { var active_link = ''; }
				var nav = '<li class="' + item.section + '"><a href="#" rel="#page-' + item.nid + '"' + active_link + '>' + item.title + '</a></li>';
				
				$('#nav ul').append(nav);
				
			});
		});
		
		$('body').on('click', '#about #nav a', function() {
			
			var target = $(this);

			$('.active').removeClass('active').removeClass('first').animate({left: '-1024px'}, 'slow', function() {
				$(this).css('left', '1024px');
			});
			$('.active_link').removeClass('active_link');
			$(target).addClass('active_link');
			$($(target).attr('rel')).addClass('active').animate({left: '0'}, 'slow');	
			
			return false;
		});
		
		
		
	});
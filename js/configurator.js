(function($) {
 	$.fn.serializeObject = function() {
 		var o = {};
 		var a = this.serializeArray();
 		$.each(a, function() {
 			if (o[this.name]) {
 				if (!o[this.name].push) {
 					o[this.name] = [o[this.name]];
 				}
 				o[this.name].push(this.value || '');
 			} else {
 				o[this.name] = this.value || '';
 			}
 		});
 		return o;
 	};
 	$(document).ready(function() {
                      
		$('#resetform').click(function() {
			$('#step1')[0].reset();
			$('#step2')[0].reset();
			$('#step2').slideUp();
			$('#step1').slideDown();
			return false;
		});
                      
    // Load configurator form items
 		$.getJSON("http://havis.um-me.com/api/configurator", function(data) {
 			$.each(data, function(key, item) {
 				// Add Vehicle Types
 				if (item.id == '156') {
 					$.each(item.children, function(key, item) {
 						$('#type_of_vehicle_elements').append('<li id="parent_' + item.id + '"><input  data-role="none" name="type_of_vehicle" id="type_of_vehicle_item_' + item.id + '" value="' + item.name + '" type="checkbox" class="radio"><label for="type_of_vehicle_item_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 					});
 				} else if (item.id == '79') {
 					$('#type_of_solution_elements').append('<li id="parent_' + item.id + '" class="parent"><input  data-role="none" name="computer_tablet_mounting" id="type_of_solution_item_' + item.id + '" value="Computer/Tablet Mounting" type="checkbox" class="checkbox"><label for="type_of_solution_item_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_solution check"></span></span><br>' + item.name + '</label></li>');
 				} else if (item.id == '103') {
 					$('#type_of_solution_elements').append('<li id="parent_' + item.id + '" class="parent"><input  data-role="none" name="equipment_mounting" id="type_of_solution_item_' + item.id + '" value="Equipment Mounting" type="checkbox" class="checkbox"><label for="type_of_solution_item_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_solution check"></span></span><br>' + item.name + '</label></li>');
 				} else if (item.id == '149') { // Power Management
 					$('#type_of_solution_elements').append('<li id="parent_' + item.id + '" class="parent"><input  data-role="none" name="power_management" id="type_of_solution_item_' + item.id + '" value="Power Management" type="checkbox" class="checkbox"><label for="type_of_solution_item_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_solution check"></span></span><br>' + item.name + '</label></li>');
 				} else if (item.id == '124') { // K9 Prisoner Transport
 					$('#type_of_solution_elements').append('<li id="parent_' + item.id + '" class="parent"><input  data-role="none" name="k9_prisoner_transport" id="type_of_solution_item_' + item.id + '" value="K9/Prisoner Transport" type="checkbox" class="checkbox"><label for="type_of_solution_item_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_solution check"></span></span><br>' + item.name + '</label></li>');
 				}
 			});
 		});
 		
 		$('body').on('click', 'input[name="type_of_vehicle"]', function() {
 			$('#type_of_solution').slideDown();
 		});
 		$('body').on('click', '#type_of_solution_item_79', function() {
 			$("#step1button").show();
 		});
 		$('body').on('click', '#type_of_solution_item_103', function() {
 			$("#step1button").show();
 		});
 		$('body').on('click', '#type_of_solution_item_149', function() {
 			$("#step1button").show();
 		});
 		$('body').on('click', '#type_of_solution_item_124', function() {
 			$("#step1button").show();
 		});
 		$("#contactinfo").submit(function() {
 			var data = $("#contactinfo").serializeObject(),
 				post = $("#contactinfo").serializeArray();
 			
 			$('.error').html('');
 			
 			if (data.fullname.length === 0) {
 				$("<p class=\"error\">Please enter in your full name.</p>").insertAfter("#fullname");
 			}
 			
 			if (data.company_name.length === 0) {
 				$("<p class=\"error\">Please enter in your company name.</p>").insertAfter("#companyname");
 			}
 			
 			if (data.email.length === 0) {
 				$("<p class=\"error\">Please enter in your email address.</p>").insertAfter("#email");
 			}
 			
 			if (data.phone_number.length === 0) {
 				$("<p class=\"error\">Please enter in your phone number.</p>").insertAfter("#phonenumber");
 			}
 			
 			if (data.fullname.length > 0 && data.company_name.length > 0 && data.email.length > 0 && data.phone_number.length > 0) {
 				$("#contactinfoanswers").text(JSON.stringify(post));
 				$("#step1").slideDown();
 				$("#contactinfo").slideUp();
 			}
 			return false;
 		});
 		$("#step1").submit(function() {
 			var data = $("#step1").serializeObject(),
 				post = $("#step1").serializeArray(),
 				items = [];
 			$("#step1answers").text(JSON.stringify(post));
 			if (data.computer_tablet_mounting == "Computer/Tablet Mounting") {
 				items.push("Computer/Tablet Mounting");
 			}
 			if (data.equipment_mounting == "Equipment Mounting") {
 				items.push("Equipment Mounting");
 			}
 			if (data.k9_prisoner_transport == "K9/Prisoner Transport") {
 				items.push("K9/Prisoner Transport");
 			}
 			if (data.power_management == "Power Management") {
 				items.push("Power Management");
 			}
 			$('#step2_more_info').append('You wanted more info on: ');
 			$.each(items, function(index, value) {
 				$('#step2_more_info').append('<div class="itemtag">' + value + '</div>');
 			});
 			$.each(items, function(index, value) {
 				$('#step2_accordian').append('<h3 id="accordian_header_' + index + '" class="ui-accordion-header">Select Options for ' + value + ' <a href="#" class="checkall" id="checkall_' + index + '" rel="' + index + '">Check all that apply</a></h3>');
 				if (value == 'Computer/Tablet Mounting') {
 					$('<div id="computer_tablet_mounting_elements" class="clearfix ui-accordion-content ui-accordion-content-' + index + '"></div>').insertAfter('#accordian_header_' + index);
 					// type of mounting
 					$.getJSON("http://havis.um-me.com/api/configurator/80", function(data) {
 						$('#computer_tablet_mounting_elements').append('<fieldset id="type_of_mounting_elements"><p class="configheader">Choose Your type of mounting</p><ul></ul></fieldset>');
 						$.each(data, function(key, item) {
 							$('#type_of_mounting_elements ul').append('<li id="parent_' + item.id + '"><input  data-role="none" name="type_of_mounting" id="type_of_mounting_' + item.id + '" value="' + item.name + '" type="checkbox" class="radio"><label for="type_of_mounting_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 						});
 						// Select Model of Computer Tablet
 						$.getJSON("http://havis.um-me.com/api/configurator/85", function(data) {
 							$('#computer_tablet_mounting_elements').append('<fieldset id="type_of_computer_tablet_elements"><p class="configheader">Choose the model of your computer or tablet</p><ul></ul></fieldset>');
 							$.each(data, function(key, item) {
 								$('#type_of_computer_tablet_elements ul').append('<li id="parent_' + item.id + '"><input  data-role="none" name="type_of_computer_tablet" id="type_of_computer_tablet_item_' + item.id + '" value="' + item.name + '" type="checkbox" class="radio"><label for="type_of_computer_tablet_item_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 							});
 							// Select Vehicle Mounting
 							$.getJSON("http://havis.um-me.com/api/configurator/90", function(data) {
 								$('#computer_tablet_mounting_elements').append('<fieldset id="vehicle_mounting_elements"><p class="configheader">Choose where in your vehicle you want to mount</p><ul></ul></fieldset>');
 								$.each(data, function(key, item) {
 									$('#vehicle_mounting_elements ul').append('<li id="parent_' + item.id + '"><input  data-role="none" name="vehicle_mounting_type" id="vehicle_mounting_type_item_' + item.id + '" value="' + item.name + '" type="checkbox" class="radio"><label for="vehicle_mounting_type_item_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 								});
 								// Other Items
 								$.getJSON("http://havis.um-me.com/api/configurator/93", function(data) {
 									$('#computer_tablet_mounting_elements').append('<fieldset id="vehicle_mounting_other_elements"><p class="configheader">Other items that you might need</p><ul></ul></fieldset>');
 									$.each(data, function(key, item) {
 										if (item.id == 95) {
 											$('#vehicle_mounting_other_elements').append('<fieldset id="vehicle_mounting_other_recommended_accessories_elements"><p class="configheader">Here are some other recommended accessories</p><ul></ul></fieldset>');
 											$.getJSON("http://havis.um-me.com/api/configurator/95", function(data) {
 												$.each(data, function(key, item) {
 													$('#vehicle_mounting_other_recommended_accessories_elements ul').append('<li id="parent_' + item.id + '"><input  data-role="none" name="vehicle_mounting_other_recommended_accessories" id="vehicle_mounting_other_recommended_accessories_item_' + item.id + '" value="' + item.name + '" type="checkbox" class="checkbox"><label for="vehicle_mounting_other_recommended_accessories_item_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 												});
 											});
 										} else {
 											$('#vehicle_mounting_other_elements ul').append('<li id="parent_' + item.id + '"><input  data-role="none" name="vehicle_mounting_other" id="vehicle_mounting_other_item_' + item.id + '" value="' + item.name + '" type="checkbox" class="checkbox"><label for="vehicle_mounting_other_item_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 										}
 									});
 								}); // other items
 							}); // end vehicle mounting
 						}); // end model of computer tablet
 					}); // end type of mounting
 				} // end type of mounting selections
 				//
 				// Start Additional Equipment
 				//
 				if (value == 'Equipment Mounting') {
 					$('<div id="equipment_mounting_elements" class="clearfix ui-accordion-content ui-accordion-content-' + index + '"></div>').insertAfter('#accordian_header_' + index);
 					// type of console
 					$.getJSON("http://havis.um-me.com/api/configurator/104", function(data) {
 						$('#equipment_mounting_elements').append('<fieldset id="type_of_console_elements"><p class="configheader">Select your type of console</p><ul></ul></fieldset>');
 						$.each(data, function(key, item) {
 							$('#type_of_console_elements ul').append('<li id="parent_' + item.id + '"><input  data-role="none" name="type_of_console" id="type_of_console_item_' + item.id + '" value="' + item.name + '" type="checkbox" class="radio"><label for="type_of_console_item_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 						});
 						// additional mounting
 						$.getJSON("http://havis.um-me.com/api/configurator/109", function(data) {
 							$('#equipment_mounting_elements').append('<fieldset id="additional_mounting_elements"><p class="configheader">Select additional mounting options</p><ul></ul></fieldset>');
 							$.each(data, function(key, item) {
 								if (item.id == 113) {
 									$('#equipment_mounting_elements').append('<fieldset id="additional_trunk_mounting_elements"><p class="configheader">Select from trunk mounts</p><ul></ul></fieldset>');
 									$.getJSON("http://havis.um-me.com/api/configurator/113", function(data) {
 										$.each(data, function(key, item) {
 											$('#additional_trunk_mounting_elements ul').append('<li id="parent_' + item.id + '"><input  data-role="none" name="additional_trunk_mounting" id="additional_trunk_mounting_item_' + item.id + '" value="' + item.name + '" type="checkbox" class="checkbox"><label for="additional_trunk_mounting_item_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 										});
 									});
 								} else { // show list of trunk mounts
 									$('#additional_mounting_elements ul').append('<li id="parent_' + item.id + '"><input  data-role="none" name="additional_mounting" id="additional_mounting_item_' + item.id + '" value="' + item.name + '" type="checkbox" class="radio"><label for="additional_mounting_item_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 								}
 							});
 							// Equipment Mounting additional accessories
 							$.getJSON("http://havis.um-me.com/api/configurator/118", function(data) {
 								$('#equipment_mounting_elements').append('<fieldset id="equipment_mounting_additional_accessories"><p class="configheader">Select additional accessories</p><ul></ul></fieldset>');
 								$.each(data, function(key, item) {
 									$('#equipment_mounting_additional_accessories ul').append('<li id="parent_' + item.id + '"><input  data-role="none" name="equipment_mounting_additional_accessories" id="equipment_mounting_additional_accessories_item_' + item.id + '" value="' + item.name + '" type="checkbox" class="radio"><label for="equipment_mounting_additional_accessories_item_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 								});
 							}); // end Equipment Mounting additional accessories
 						}); // end additional mounting
 					}); // end type of console
 				}
 				//
 				// Start K9/Prisoner Transport 
 				//
 				if (value == 'K9/Prisoner Transport') {
 					$('<div id="k9_prisoner_transport_elements" class="clearfix ui-accordion-content ui-accordion-content-' + index + '"></div>').insertAfter('#accordian_header_' + index);
 					$('#k9_prisoner_transport_elements').append('<fieldset><p class="highlighted">K9/Prisoner Transport. Choose the option that best suits your transport set up.</p>');
 					$('#k9_prisoner_transport_elements').append('<fieldset id="k9_unit_assessories_elements"><p class="configheader">K9 Unit Assessories</p><ul></ul></fieldset>');
 					$('#k9_prisoner_transport_elements').append('<fieldset id="k9_prisoner_transport_unit_accessories_elements"><p class="configheader">K9 Prisoner Transport Unit Accessories</p><ul></ul></fieldset>');
 					$('#k9_prisoner_transport_elements').append('<fieldset id="prisoner_transport_elements"><p class="configheader">Prisoner Transport</p><ul></ul></fieldset>');
 					$('#k9_prisoner_transport_elements').append('</fieldset>');
 					// K9 Unit Assessories
 					$.getJSON("http://havis.um-me.com/api/configurator/126", function(data) {
 						$.each(data, function(key, item) {
 							$('#k9_unit_assessories_elements ul').append('<li id="parent_' + item.id + '"><input  data-role="none" name="k9_unit_assessories_elements" id="k9_unit_assessories_elements_items_' + item.id + '" value="' + item.name + '" type="checkbox" class="radio"><label for="k9_unit_assessories_elements_items_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 						});
 					}); // end K9 Unit Assessories
 					// K9 Unit Assessories
 					$.getJSON("http://havis.um-me.com/api/configurator/128", function(data) {
 						$.each(data, function(key, item) {
 							$('#k9_prisoner_transport_unit_accessories_elements ul').append('<li id="parent_' + item.id + '"><input  data-role="none" name="k9_prisoner_transport_unit_accessories_elements" id="k9_prisoner_transport_unit_accessories_elements_items_' + item.id + '" value="' + item.name + '" type="checkbox" class="radio"><label for="k9_prisoner_transport_unit_accessories_elements_items_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 						});
 					}); // end K9 Unit Assessories
 					// K9 Unit Assessories
 					$.getJSON("http://havis.um-me.com/api/configurator/130", function(data) {
 						$.each(data, function(key, item) {
 							$('#prisoner_transport_elements ul').append('<li id="parent_' + item.id + '"><input  data-role="none" name="prisoner_transport_elements" id="prisoner_transport_elements_items_' + item.id + '" value="' + item.name + '" type="checkbox" class="radio"><label for="prisoner_transport_elements_items_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 						});
 					}); // end K9 Unit Assessories					
 				}
 				//
 				// Start Power Management 
 				//
 				if (value == 'Power Management') {
 					$('<div id="power_management_elements" class="clearfix ui-accordion-content ui-accordion-content-' + index + '"><fieldset><p class="configheader">Power Management</p><ul></ul></fieldset></div>').insertAfter('#accordian_header_' + index);
 					$.getJSON("http://havis.um-me.com/api/configurator/149", function(data) {
 						$.each(data, function(key, item) {
 							$('#power_management_elements fieldset ul').append('<li id="parent_' + item.id + '"><input  data-role="none" name="power_management_elements" id="power_management_elements_items_' + item.id + '" value="' + item.name + '" type="checkbox" class="radio"><label for="power_management_elements_items_' + item.id + '"><span class="imgwrap"><img src="' + item.image + '" class="shadow img" alt="' + item.name + '"><span class="type_of_vehicle_check check"></span></span><br>' + item.name + '</label></li>');
 						});
 					});
 				}
 			});
// 			$('#step2_accordian').accordion({
// 				heightStyle: "content"
// 			});
 			$("#step1").slideUp();
 			$("#step2").slideDown();
 			return false;
 		});

 		$("#step2").submit(function() {
 			var data = $("#step2").serializeObject(),
 					post = $("#step2").serializeArray();
 			
 			$("#step2answers").text(JSON.stringify(post));

 			var step1answers = $.parseJSON($("#step1answers").text()),
 					step2answers = $.parseJSON($("#step2answers").text()),
 					contactinfoanswers = $.parseJSON($("#contactinfoanswers").text());

 			$("#step2").slideUp();
 			$("#summarydiv").slideDown();
 			
			$.each( contactinfoanswers, function( key, value ) {
			    var label = value.name.replace(/_/g," ");
			    var label = label.charAt(0).toUpperCase() + label.slice(1);
			    $('#summary_contactinfo').append("<strong>" + label + ':</strong> ' + value.value + '\n<br>');
			});
			
			$.each( step1answers, function( key, value ) {
				var label = value.name.replace(/_/g," ");
				var label = label.charAt(0).toUpperCase() + label.slice(1);
			  
			  if (value.name == 'type_of_vehicle') {
					$('#summary_vehicle').append("<strong>" + label + ':</strong> ' + value.value + '\n<br>');
			  } else {
					$('#summary_vehicle').append("<strong>" + label + ':</strong> Yes\n<br>');				  
			  }
			});
			
			$.each( step2answers, function( key, value ) {
			     var label = value.name.replace(/_/g," ");
			    var label = label.charAt(0).toUpperCase() + label.slice(1);
			    $('#summary_results').append("<strong>" + label + ':</strong> ' + value.value + '\n<br>');
			});
			var from = $('#email').val(),
					message = $('#summarydiv .wrapper').html();
			
			$.ajax({
				url: 'http://havis.um-me.com/api/send-mail',
				data: 'from=' + from + '&message=' + message,
				success: function(data) {
					$('#summarydiv .wrapper').prepend('<div class="prompt">Your configuration has been sent. We will be in touch with you soon.</div>');
				}
			});
			 			
 			return false;
 			});
 			
		$('body').on('click', '.checkall', function() {
			var elementid = $(this).attr('rel');
			$('.ui-accordion-content-' + elementid).find('input:checkbox').prop('checked', true);
			console.log(elementid);
			return false;
		}); 			
 	});
 })(jQuery);